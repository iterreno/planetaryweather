# Planetary climate #
----
## This project has the following capabilities and objectives: ##
* To be able to predict the weather in the next 10 years.
* Have a REST API which returns in JSON format the climatic condition of the day consulted.
* A database with the daily weather conditions.
* Generates a data model with the conditions of every day up to 10 years and up using a job to calculate them. Which runs at 12AM every day updating the database with the data.
* Answer the following questions.
* Inform How many periods of drought will there be?
* Inform How many periods of rain will there be?
* Inform What day will be the maximum rain peak?
* Inform How many periods of optimal pressure and temperature conditions will there be?
* Responsive presentation for web and mobile theme of Star Trek

[Explanation of the Use Case to implement](https://bitbucket.org/iterreno/planetaryweather/wiki/Home)

# Clima planetario #
----
## Este proyecto tiene las siguientes capacidades y objetivos: ##

* Poder predecir el clima en los próximos 10 años.
* Disponer una API REST la cual devuelve en formato JSON la condición climática del día consultado.
* Una base de datos con las condiciones meteorológicas de todos los días.
* Genera un modelo de datos con las condiciones de todos los días hasta 10 años en adelante utilizando un job para calcularlas. El cual corre a las 12AM todos los días actualizando la base de datos con los datos.
* Responde a las siguientes consultas.
* Informar ¿Cuántos períodos de sequía habrá?
* Informar ¿Cuántos períodos de lluvia habrá ?
* Informar ¿Qué día será el pico máximo de lluvia ?
* Informar ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?
* Presentación responsiva para web y mobile tematica de Star Trek


[Explicación del Caso de uso para implementar](https://bitbucket.org/iterreno/planetaryweather/wiki/Home)