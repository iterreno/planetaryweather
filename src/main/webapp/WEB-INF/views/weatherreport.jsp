<%@ page session="false" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<!--
	Solid State by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>${appName}</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<c:url value="/resources/assets/css/main.css" />" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header">
						<h1><a href="<c:url value="/" />">${appName}</a></h1>
						<nav>
							<a href="#menu">Menu</a>
						</nav>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<div class="inner">
							<h2>Menu</h2>
							<ul class="links">
								<li><a href="<c:url value="/" />">Inicio</a></li>
								<li><a href="<c:url value="/weatherreport" />">Pronostico del clima</a></li>
								<li><a target="_blank" href="<c:url value="/v1/clima?dia=566" />">Servicio para civilizaciones REST API</a></li>
								<li><a target="_blank" href="https://bitbucket.org/iterreno/planetaryweather/overview">Ver repositorio en bitbucket</a></li>
							</ul>
							<a href="#" class="close">Cerrar</a>
						</div>
					</nav>

				<!-- Wrapper -->
					<section id="wrapper">
						<header>
							<div class="inner">
								<h2>Pronostico del clima a 10 años</h2>
								<p>Predicción del clima en base a la posición de los tres planetas.</p>
							</div>
						</header>

						<!-- Content -->
							<div class="wrapper">
								<div class="inner">

									<section>
										<h3 class="major">Condiciones climaticas</h3>
										<div class="row">
											<div class="12u 12u$(medium)">
												<h4>Escenarios</h4>
												<ul class="alt">
													<li>Cuando los tres planetas están alineados entre sí y a su vez alineados con respecto al sol, el sistema solar experimenta un período de sequía..</li>
													<li>Cuando los tres planetas no están alineados, forman entre sí un triángulo. Es sabido que en el momento en el que el sol se encuentra dentro del triángulo, el sistema solar experimenta un período de lluvia, teniendo éste, un pico de intensidad cuando el perímetro del triángulo está en su máximo.</li>
													<li>Las condiciones óptimas de presión y temperatura se dan cuando los tres planetas están alineados entre sí pero no están alineados con el sol..</li>
												</ul>
											</div>
											
										</div>
									</section>
									<section>
										<h3 class="major">Información de planetas</h3>
										<h4>Detalle</h4>
										<div class="table-wrapper">
											<table>
												<thead>
													<tr>
														<th>Nombre del Planeta</th>
														<th>Distacia al sol/Km</th>
														<th>Grados de rotacion por dia</th>
														<th title="Se saco con una regla de tres simple">Dias para vuelta completa orbital</th>
														<th>Direccion agujas del relog</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="planet" items="${planets}">
														<tr>
															<td>${planet.name}</td>
															<td>${planet.distanceXSun}</td>
															<td>${planet.degreeRotacionXday}</td>
															<td>${planet.completeOrbitalLapInDays}</td>
															<td>
																<c:choose>
																    <c:when test="${planet.rotationClockwiseClockDirection}">
																       Si
																    </c:when>    
																    <c:otherwise>
																        No
																    </c:otherwise>
																</c:choose>
															</td>
														</tr>
													</c:forEach>
												</tbody>
												
											</table>
										</div>
										<h3 class="major">Calculos generales de planetas</h3>
										<h4>Detalle</h4>
										<div class="table-wrapper">
											<table>
												<thead>
													<tr>
														<th>Cantidad de Días a 10 años</th>
														<th>¿Cuántos períodos de sequía habrá?</th>
														<th>¿Cuántos períodos de lluvia habrá ?</th>
														<th>¿Qué día será el pico máximo de lluvia ?</th>
														<th>¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>${daysToTenYears}</td>
														<td>${counterDrought}</td>
														<td>${counterRain}</td>
														<td>${counterMaxRain}</td>
														<td>${counterPerfectConditionsWeather}</td>
													</tr>
												</tbody>
												
											</table>
										</div>
										
										<div class="table-wrapper">
											<table class="alt">
												<thead>
													<tr>
														<th>Dìa</th>
														<th>Planetas alineados</th>
														<th>Planetas y Sol alineados</th>
														<th>Pronostico</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="resultPlanetsPosition" items="${pronosticPlanetPositions}">
														<tr>
															<td>${resultPlanetsPosition.timeInDay}</td>
															<td>
																<c:choose>
																    <c:when test="${resultPlanetsPosition.planetsAlignment}">
																       Si
																    </c:when>    
																    <c:otherwise>
																        No
																    </c:otherwise>
																</c:choose>
															</td>
															<td>
																<c:choose>
																    <c:when test="${resultPlanetsPosition.planetsAndSunAlignment}">
																        Si
																    </c:when>    
																    <c:otherwise>
																         No
																    </c:otherwise>
																</c:choose>
															</td>
															<td>
																${resultPlanetsPosition.resultWeatherReportText}
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
										
										</section>

								</div>
							</div>

					</section>

			</div>

		<!-- Scripts -->
			<script src="<c:url value="/resources/assets/js/skel.min.js" />"></script>
			<script src="<c:url value="/resources/assets/js/jquery.min.js" />"></script>
			<script src="<c:url value="/resources/assets/js/jquery.scrollex.min.js" />"></script>
			<script src="<c:url value="/resources/assets/js/util.js" />"></script>
			<!--[if lte IE 8]><script src="<c:url value="/resources/assets/js/ie/respond.min.js" />"></script><![endif]-->
			<script src="<c:url value="/resources/assets/js/main.js" />"></script>

	</body>
</html>