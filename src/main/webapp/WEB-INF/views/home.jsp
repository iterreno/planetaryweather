<%@ page session="false" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<!--
	Solid State by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>${appName}</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<c:url value="/resources/assets/css/main.css" />" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<h1><a href="index.html">${appName}</a></h1>
						<nav>
							<a href="#menu">Menu</a>
						</nav>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<div class="inner">
							<h2>Menu</h2>
							<ul class="links">
								<li><a href="<c:url value="/" />">Inicio</a></li>
								<li><a href="<c:url value="/weatherreport" />">Pronostico del clima</a></li>
								<li><a target="_blank" href="<c:url value="/v1/clima?dia=566" />">Servicio para civilizaciones REST API</a></li>
								<li><a target="_blank" href="https://bitbucket.org/iterreno/planetaryweather/overview">Ver repositorio en bitbucket</a></li>
							</ul>
							<a href="#" class="close">Cerrar</a>
						</div>
					</nav>

				<!-- Banner -->
					<section id="banner">
						<div class="inner">
							<div class="logo"><span class="icon fa-diamond"></span></div>
							<h2>DOMINAMOS LA PREDICCIÓN DEL CLIMA DEL SISTEMA SOLAR VULCANOS, FERENGIS Y BETAZOIDES .</h2>
							<p>PODEMOS PREDECIR EL CLIMA EN LOS PRÓXIMOS 10 AÑOS DEL SISTEMA SOLAR VULCANOS, FERENGIS Y BETAZOIDES. <a target="_blank" href="https://www.linkedin.com/in/ismael-terreno/"> Autor - Ismael Terreno</a></p>
						</div>
					</section>

				<!-- Wrapper -->
					<section id="wrapper">

						<!-- One -->
							<section id="one" class="wrapper spotlight style1">
								<div class="inner">
									<a href="#" class="image"><img src="<c:url value="/resources/images/ferengi-culture.jpg" />" alt="" /></a>
									<div class="content">
										<h2 class="major">Ferengi</h2>
										<p>
											Su cultura y psicología se caracteriza por una obsesión capitalista hacia el comercio y los beneficios. También son conocidos por su misoginia, de forma que sus mujeres tienen prohibido realizar cualquier tipo de transacción económica e ir vestidas en público. Proceden de Ferenginar, un planeta caracterizado por lluvias constantes, que está regido por el gran Negus, el ferengi más ambicioso, que tiene la enorme responsabilidad de que su ambición personal refleje la ambición pública. Como la mayor parte de su cultura, su religión está basada en el capitalismo.
										</p>
										<a target="_blank" href="https://es.wikipedia.org/wiki/Ferengi" class="special">Aprender mas</a>
									</div>
								</div>
							</section>

						<!-- Two -->
							<section id="two" class="wrapper alt spotlight style2">
								<div class="inner">
									<a href="#" class="image"><img src="<c:url value="/resources/images/vulcano-culture.jpeg" />" alt="" /></a>
									<div class="content">
										<h2 class="major">Vulcano</h2>
										<p>
										Alcanzada la adolescencia, los vulcanos se someten a ciertos ritos de paso, entre los que destacan el kolinahr, cuyo propósito es suprimir totalmente las emociones, y el tal’oth, referido a las habilidades de supervivencia del joven iniciado, el cual debe permanecer totalmente aislado durante cuatro meses en el desierto, armado sólo con una espada ceremonial para defenderse o conseguir alimentos.
										</p>
										<a target="_blank" href="https://es.wikipedia.org/wiki/Vulcano_(Star_Trek)" class="special">Aprender mas</a>
									</div>
								</div>
							</section>

						<!-- Three -->
							<section id="three" class="wrapper spotlight style3">
								<div class="inner">
									<a href="#" class="image"><img src="<c:url value="/resources/images/betazoides-culture.jpeg" />" alt="" /></a>
									<div class="content">
										<h2 class="major">Betazoide</h2>
										<p>
											La sociedad de Betazed tiende a tradiciones más formales y detalladas, con ceremonias que la mayoría de las culturas han eliminado en su fase evolutiva actual, como la ceremonia de boda tradicional en la que no se permite ninguna ropa.
										</p>
										<a target="_blank" href="https://es.wikipedia.org/wiki/Betazoide" class="special">Learn more</a>
									</div>
								</div>
							</section>

						

					</section>

				<!-- Footer -->
					<section id="footer">
						<div class="inner">

							<ul class="copyright">
								<li>&copy; Vaoru.</li><li>Design: <a href="http://www.vaoru.com">Vaoru Consulting</a></li>
							</ul>
						</div>
					</section>

			</div>

		<!-- Scripts -->
			<script src="<c:url value="/resources/assets/js/skel.min.js" />"></script>
			<script src="<c:url value="/resources/assets/js/jquery.min.js" />"></script>
			<script src="<c:url value="/resources/assets/js/jquery.scrollex.min.js" />"></script>
			<script src="<c:url value="/resources/assets/js/util.js" />"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<c:url value="/resources/assets/js/main.js" />"></script>
	</body>
</html>