package com.vaoru.climaplanetario.rest.api;

import org.springframework.web.bind.annotation.RequestParam;

import com.vaoru.climaplanetario.dto.DayReportDTO;


public interface WeatherRest {
	/**
	 * Get the Weather day report by requested day.
	 * @param day Weather day report.
	 * @return The Weather day report.
	 */
	
	DayReportDTO dayReport(@RequestParam(value="dia", required=false) String day);
}
