package com.vaoru.climaplanetario.rest.api.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.vaoru.climaplanetario.dto.DayReportDTO;
import com.vaoru.climaplanetario.model.ResultPlanetsPosition;
import com.vaoru.climaplanetario.rest.api.WeatherRest;
import com.vaoru.climaplanetario.service.WeatherService;

@RestController("WeatherRestAPI")
@EnableWebMvc
@RequestMapping("/v1")
public class WeatherRestController implements WeatherRest{
	
	@Autowired
	private WeatherService weatherService;
	
	@Override
	@RequestMapping(value = "/clima", method = RequestMethod.GET)
	public DayReportDTO dayReport(@RequestParam(value="dia", required=false) String day) {
		DayReportDTO dayReport = new DayReportDTO();
		ResultPlanetsPosition result = weatherService.dayReport(Integer.valueOf(day)-1);
		dayReport.setDia(String.valueOf(result.getTimeInDay()));
		dayReport.setClima(result.getResultWeatherReportText());
		return dayReport;
	}

}
