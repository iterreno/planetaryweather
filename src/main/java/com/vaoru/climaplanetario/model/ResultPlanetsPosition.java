package com.vaoru.climaplanetario.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.vaoru.climaplanetario.enums.ConfigApp;

@Entity
@Table(name = "Result_Planets_Position")
public class ResultPlanetsPosition {
	
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;
	
	/**
	 * Planets to work.
	 */
	@Transient
	private List<Planet> planets;
	/**
	 * Day of the period of years.
	 */
	@Column(name = "timeInDay")
	private int timeInDay;
	/**
	 * If all the planets are alignment on a single line.
	 */
	@Column(name = "planetsAlignment")
	private boolean planetsAlignment;
	
	/**
	 * If all the planets and the sun are alignment on a single line.
	 */
	@Column(name = "planetsAndSunAlignment")
	private boolean planetsAndSunAlignment;
	
	/**
	 * If the sun is inside the triangle formed by the planets.
	 */
	@Column(name = "sunInsideTrianglePlanets")
	private boolean sunInsideTrianglePlanets;
	
	/**
	 * If the sun is center inside the triangle formed by the planets.
	 */
	@Column(name = "sunInsideMaxRainTrianglePlanets")
	private boolean sunInsideMaxRainTrianglePlanets;
	
	/**
	 * If the sun is outside the triangle formed by the planets.
	 */
	@Column(name = "sunOutsideTrianglePlanets")
	private boolean sunOutsideTrianglePlanets;
	/**
	 * Creation date.
	 */
	@Column(name = "creation")
	Date creation;
	
	@Transient
	private String resultWeatherReport;
	
	public List<Planet> getPlanets() {
		return planets;
	}
	public void setPlanets(List<Planet> planets) {
		this.planets = planets;
	}
	public int getTimeInDay() {
		return timeInDay;
	}
	public void setTimeInDay(int timeInDay) {
		this.timeInDay = timeInDay;
	}
	
	public void setPlanetsAlignment(boolean planetsAlignment) {
		this.planetsAlignment = planetsAlignment;
	}
	public boolean isPlanetsAlignment() {
		return planetsAlignment;
	}
	
	public boolean isPlanetsAndSunAlignment() {
		return planetsAndSunAlignment;
	}
	public void setPlanetsAndSunAlignment(boolean planetsAndSunAlignment) {
		this.planetsAndSunAlignment = planetsAndSunAlignment;
	}
	public boolean isSunInsideTrianglePlanets() {
		return sunInsideTrianglePlanets;
	}
	public void setSunInsideTrianglePlanets(boolean sunInsideTrianglePlanets) {
		this.sunInsideTrianglePlanets = sunInsideTrianglePlanets;
	}
	public boolean isSunInsideMaxRainTrianglePlanets() {
		return sunInsideMaxRainTrianglePlanets;
	}
	public void setSunInsideMaxRainTrianglePlanets(boolean sunInsideCenterTrianglePlanets) {
		this.sunInsideMaxRainTrianglePlanets = sunInsideCenterTrianglePlanets;
	}
	public boolean isSunOutsideTrianglePlanets() {
		return sunOutsideTrianglePlanets;
	}
	public void setSunOutsideTrianglePlanets(boolean sunOutsideTrianglePlanets) {
		this.sunOutsideTrianglePlanets = sunOutsideTrianglePlanets;
	}
	
	public String getResultWeatherReportText() {
		
		if ( !this.isPlanetsAndSunAlignment() && this.isPlanetsAlignment()){
			resultWeatherReport = ConfigApp.PERFECT_CONDITIONS_WEATHER.getValue();
		}else if(this.isPlanetsAndSunAlignment() && this.isPlanetsAndSunAlignment()){
			resultWeatherReport =ConfigApp.DROUGHT.getValue();
		}else if(this.isSunInsideTrianglePlanets()){
			resultWeatherReport =ConfigApp.RAIN.getValue();
		}else if(this.isSunInsideMaxRainTrianglePlanets()){
			resultWeatherReport =ConfigApp.MAX_RAIN.getValue();
		}else{
			resultWeatherReport =ConfigApp.SUN.getValue();
		}
		return resultWeatherReport;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public Date getCreation() {
		return creation;
	}
	public void setCreation(Date creation) {
		this.creation = creation;
	}
	
	
	
}
