package com.vaoru.climaplanetario.model;
/**
 * Base for the planets and sun.
 * @author ismaelterreno
 *
 */
public class SolarSystem {
	
	private Point coordinates;
	
	public Point getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Point coordinates) {
		this.coordinates = coordinates;
	}
}
