package com.vaoru.climaplanetario.model;


public class Planet extends SolarSystem{
	private String name;
	private int degreeRotacionXday;
	private int distanceXSun;
	private boolean  isRotationClockwiseClockDirection;
	
	
	public int getDegreeRotacionXday() {
		return degreeRotacionXday;
	}
	public void setDegreeRotacionXday(int degreeRotacionXday) {
		this.degreeRotacionXday = degreeRotacionXday;
	}
	public int getDistanceXSun() {
		return distanceXSun;
	}
	public void setDistanceXSun(int distanceXSun) {
		this.distanceXSun = distanceXSun;
	}
	public boolean isRotationClockwiseClockDirection() {
		return isRotationClockwiseClockDirection;
	}
	public void setRotationClockwiseClockDirection(boolean isRotationClockwiseClockDirection) {
		this.isRotationClockwiseClockDirection = isRotationClockwiseClockDirection;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public float getCompleteOrbitalLapInDays(){
		int unitTime = 1;
		int completeDegreeRotation = 360;
		float lapInDays = (completeDegreeRotation * unitTime) /  getDegreeRotacionXday() ;
		return lapInDays;
	}
	
}
