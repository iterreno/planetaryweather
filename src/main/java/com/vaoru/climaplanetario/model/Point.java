package com.vaoru.climaplanetario.model;

/**
 * Representen a point in a two-dimensional plane.
 * @author ismaelterreno
 *
 */
public class Point {
	/**
	 * Coordinates for x Axis in a two-dimensional plane.
	 */
	private double xAxis;
	/**
	 * Coordinates for y Axis in a two-dimensional plane.
	 */
	private double yAxis;
	public double getxAxis() {
		return xAxis;
	}
	public void setxAxis(double xAxis) {
		this.xAxis = xAxis;
	}
	public double getyAxis() {
		return yAxis;
	}
	public void setyAxis(double yAxis) {
		this.yAxis = yAxis;
	}
}
