package com.vaoru.climaplanetario.model;

public class Star extends SolarSystem{
	private double radio;

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}
	
}
