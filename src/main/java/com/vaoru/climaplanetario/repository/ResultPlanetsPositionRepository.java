package com.vaoru.climaplanetario.repository;

import org.springframework.data.repository.CrudRepository;

import com.vaoru.climaplanetario.model.ResultPlanetsPosition;

public interface ResultPlanetsPositionRepository extends CrudRepository<ResultPlanetsPosition, Integer> {
	
}
