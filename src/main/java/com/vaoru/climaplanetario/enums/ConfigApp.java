package com.vaoru.climaplanetario.enums;

public enum ConfigApp {
	APP_NAME("Clima Planetario"),
    YES("Si"),
    NO("Si"),
    PERFECT_CONDITIONS_WEATHER("Condiciones óptimas de presión y temperatura"),
    DROUGHT("Sequìa"), RAIN("Lluvia"), MAX_RAIN("Pico máximo de lluvia"), SUN("Sol")
    ;

    private String value;

    private ConfigApp(String name) {
        this.value = name;
    }

    public String getValue() {
        return value;
    }
}
