package com.vaoru.climaplanetario;

import java.util.Calendar;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.vaoru.climaplanetario.enums.ConfigApp;
import com.vaoru.climaplanetario.service.WeatherService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	
	
	
	@Autowired
	private WeatherService weatherService;
	
	@PostConstruct
	private void init(){
		
	}
	
	/**
	 * Weather Report view.
	 */
	@RequestMapping(value = "/weatherreport", method = RequestMethod.GET)
	public String weatherreport(Locale locale, Model model) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)+10);
		int daysToTenYears = weatherService.daysToTenYears(new LocalDate());
		model.addAttribute("appName", ConfigApp.APP_NAME.getValue());
		model.addAttribute("PERFECT_CONDITIONS_WEATHER", ConfigApp.PERFECT_CONDITIONS_WEATHER.getValue());
		model.addAttribute("DROUGHT", ConfigApp.DROUGHT.getValue());
		model.addAttribute("YES", ConfigApp.YES.getValue());
		model.addAttribute("NO", ConfigApp.NO.getValue());
		model.addAttribute("planets", weatherService.getPlanets());
		model.addAttribute("daysToTenYears", daysToTenYears);
		model.addAttribute("pronosticPlanetPositions", weatherService.pronosticPlanetPositions(daysToTenYears));
		model.addAttribute("counterDrought", weatherService.getLastCounterResultsReport().getCounterDrought());
		model.addAttribute("counterPerfectConditionsWeather", weatherService.getLastCounterResultsReport().getCounterPerfectConditionsWeather());
		model.addAttribute("counterRain", weatherService.getLastCounterResultsReport().getCounterRain());
		model.addAttribute("counterMaxRain", weatherService.getLastCounterResultsReport().getCounterMaxRain());
		return "weatherreport";
	}
	
	/**
	 * Weather Report view.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "home";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
