package com.vaoru.climaplanetario.dto;

public class CounterResultsReport {
	private int counterDrought;
	private int counterPerfectConditionsWeather;
	private int counterRain;
	private int counterMaxRain;
	
	public int getCounterDrought() {
		return counterDrought;
	}
	public void setCounterDrought(int counterDrought) {
		this.counterDrought = counterDrought;
	}
	public int getCounterPerfectConditionsWeather() {
		return counterPerfectConditionsWeather;
	}
	public void setCounterPerfectConditionsWeather(int counterPerfectConditionsWeather) {
		this.counterPerfectConditionsWeather = counterPerfectConditionsWeather;
	}
	public int getCounterRain() {
		return counterRain;
	}
	public void setCounterRain(int counterRain) {
		this.counterRain = counterRain;
	}
	public int getCounterMaxRain() {
		return counterMaxRain;
	}
	public void setCounterMaxRain(int counterMaxRain) {
		this.counterMaxRain = counterMaxRain;
	}
	
}
