package com.vaoru.climaplanetario.dto;

public class DayReportDTO {
	private String dia;
	private String clima;
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getClima() {
		return clima;
	}
	public void setClima(String clima) {
		this.clima = clima;
	}
	
}
