package com.vaoru.climaplanetario.service;

import java.util.List;

import org.joda.time.LocalDate;

import com.vaoru.climaplanetario.dto.CounterResultsReport;
import com.vaoru.climaplanetario.model.Planet;
import com.vaoru.climaplanetario.model.ResultPlanetsPosition;

public interface WeatherService {
	
	ResultPlanetsPosition dayReport(int day);
	/**
	 * Pronostic Planet Positions for a given time in days
	 * @return List of results day by day.
	 */
	List<ResultPlanetsPosition> pronosticPlanetPositions(int daysToWork);
	/**
	 * Give the amount of days from one date to another adding 10 years.
	 * @param fromDate
	 * @return Amount of days
	 */
	int daysToTenYears(LocalDate fromDate) ;
	
	List<Planet> getPlanets() ;
	/**
	 * Get last report from the last pronostic executed
	 * @see WeatherService#pronosticPlanetPositions(int)
	 * @return
	 */
	CounterResultsReport getLastCounterResultsReport();
	/**
	 * Pronostic Planet Positions from today to 10 years forward.
	 * @return List of results day by day.
	 */
	void jobBatchServiceReport();
}
