package com.vaoru.climaplanetario.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vaoru.climaplanetario.HomeController;
import com.vaoru.climaplanetario.dto.CounterResultsReport;
import com.vaoru.climaplanetario.model.Planet;
import com.vaoru.climaplanetario.model.Point;
import com.vaoru.climaplanetario.model.ResultPlanetsPosition;
import com.vaoru.climaplanetario.model.Star;
import com.vaoru.climaplanetario.repository.ResultPlanetsPositionRepository;
import com.vaoru.climaplanetario.service.WeatherService;

@Service
public class WeatherServiceImpl implements WeatherService{
	
	private static final Logger logger = LoggerFactory.getLogger(WeatherServiceImpl.class);
	private List<Planet> planets;
	private Star sun;
	private CounterResultsReport lastCounterResultsReport;
	@Autowired
	private ResultPlanetsPositionRepository planetsPositionRepository;
	/**
	 * Loads the planets information to work.
	 */
	private void loadPlanets() {
		planets = new ArrayList<Planet>();
		Planet ferengi = new Planet();
		ferengi.setName("Ferengi");
		ferengi.setDistanceXSun(500);
		ferengi.setDegreeRotacionXday(1);
		ferengi.setRotationClockwiseClockDirection(true);
		planets.add(ferengi);
		Planet vulcanos = new Planet();
		vulcanos.setName("Vulcanos");
		vulcanos.setDistanceXSun(1000);
		vulcanos.setDegreeRotacionXday(5);
		vulcanos.setRotationClockwiseClockDirection(false);
		planets.add(vulcanos);
		Planet betasoide = new Planet();
		betasoide.setName("Betasoide");
		betasoide.setDistanceXSun(2000);
		betasoide.setDegreeRotacionXday(3);
		betasoide.setRotationClockwiseClockDirection(true);
		planets.add(betasoide);
		sun = new Star();
	}
	
	
	
	public List<Planet> getPlanets() {
		return planets;
	}
	
	public CounterResultsReport getLastCounterResultsReport() {
		return lastCounterResultsReport;
	}



	@PostConstruct
	private void init(){
		lastCounterResultsReport = new CounterResultsReport();
		loadPlanets();
	}
	
	/**
	 * Clean the last counter report executed.
	 */
	private void clearLastCounterReport(){
		lastCounterResultsReport.setCounterDrought(0);
		lastCounterResultsReport.setCounterPerfectConditionsWeather(0);
		lastCounterResultsReport.setCounterRain(0);
		lastCounterResultsReport.setCounterMaxRain(0);
	}
	
	public ResultPlanetsPosition dayReport(int day){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, Calendar.YEAR);
		// Just work one day report.
		calendar.set(Calendar.YEAR, Calendar.DAY_OF_MONTH + 1);
		int daysToTenYears = daysToTenYears(new LocalDate());
		List<ResultPlanetsPosition> resultPlanetsPositions = pronosticPlanetPositions(daysToTenYears);
		return resultPlanetsPositions.get(day);
	}
	
	public int daysToTenYears(LocalDate fromDate) {
	  LocalDate newYear = fromDate.plusYears(10).withDayOfYear(1);
	  return Days.daysBetween(fromDate, newYear).getDays();
	}
	
	
	/**
	 * Pronostic Planet Positions for a given time in days
	 * @return
	 */
	public List<ResultPlanetsPosition> pronosticPlanetPositions(int daysToWork){
		clearLastCounterReport();
		List<ResultPlanetsPosition> resultPlanetsPositions = new ArrayList<ResultPlanetsPosition>();
		ResultPlanetsPosition result;
		for (int i=1; i < (daysToWork+1); i++){
			result = new ResultPlanetsPosition();
			result.setPlanets(planets);
			result.setTimeInDay(i);
			result.setPlanetsAlignment(calculatePlanetsAligment(result.getPlanets(),i));
			result.setPlanetsAndSunAlignment(calculateSunAndPlanetsAlignment(result.getPlanets(),i));
			result = calculateSunLocationVsTrianglePlanets(planets, i,result);
			
			if ( !result.isPlanetsAndSunAlignment() && result.isPlanetsAlignment()){
				lastCounterResultsReport.setCounterPerfectConditionsWeather(
						lastCounterResultsReport.getCounterPerfectConditionsWeather()+1
				);
			}else if(result.isPlanetsAndSunAlignment() && result.isPlanetsAndSunAlignment()){
				lastCounterResultsReport.setCounterDrought(
						lastCounterResultsReport.getCounterDrought()+1
				); 
			}
			if(result.isSunInsideTrianglePlanets()){
				lastCounterResultsReport.setCounterRain(
						lastCounterResultsReport.getCounterRain()+1
				);
			}
			if(result.isSunInsideMaxRainTrianglePlanets()){
				lastCounterResultsReport.setCounterMaxRain(
						lastCounterResultsReport.getCounterMaxRain()+1
				);
			}
			resultPlanetsPositions.add(result);
		}
		return resultPlanetsPositions;
	}
	
	/**
	 * Calculate the alignment of three planets.
	 * @param planets
	 * @param day
	 * @return
	 */
	private boolean calculatePlanetsAligment(List<Planet> planets, int day){
		boolean calculatePlanetsAligment;
		
		List<Planet> planetsToday = calculatePlanetsCoordinatesByCurrentDay(planets,day);
		
		// First vector based on coordinates.
		double slope1 = (
							planetsToday.get(1).getCoordinates().getyAxis() - planetsToday.get(0).getCoordinates().getyAxis()
						) 
						/ 
						(
							planetsToday.get(1).getCoordinates().getxAxis() - planetsToday.get(0).getCoordinates().getxAxis()
						);
		// Second vector based on coordinates.
		double slope2 = (
							planetsToday.get(2).getCoordinates().getyAxis() - planetsToday.get(1).getCoordinates().getyAxis()
						) 
						/ 
						(
							planetsToday.get(2).getCoordinates().getxAxis() - planetsToday.get(1).getCoordinates().getxAxis()
						);
		// Are formed vector alignment.
		if (slope1 == slope2) {
			 calculatePlanetsAligment = true;
		}else{
			 calculatePlanetsAligment = false;
		} 
		return calculatePlanetsAligment;
	}
	
	/**
	 * Calculate the alignment of three planets and the sun with a unique vector to the same direction.
	 * @param planets
	 * @param day
	 * @return
	 */
	private boolean calculateSunAndPlanetsAlignment(List<Planet> planets, int day){
		boolean calculatePlanetsSunAligment=false;
		if(calculatePlanetsAligment(planets,day)){
			Point sunRadioCoordinates = calculateSunRadio(planets, day);
			Planet sun = new Planet();
			sun.setCoordinates(sunRadioCoordinates);
			List<Planet> firstPlanetsAndSun = new ArrayList<Planet>();
			firstPlanetsAndSun.add(planets.get(1));
			firstPlanetsAndSun.add(planets.get(2));
			firstPlanetsAndSun.add(sun);
			// If the already alignment first two planets are also alignment to the sun.
			if(calculatePlanetsAligment(firstPlanetsAndSun,day)){
				calculatePlanetsSunAligment = true;
			}else{
				calculatePlanetsSunAligment = false;
			}
		}
		return calculatePlanetsSunAligment;
	}
	
	/**
	 * Calculate the sun's radio with the three points of the circle formed by the orbit.
	 * @see http://es.wikihow.com/calcular-el-radio-de-un-c%C3%ADrculo
	 * @see https://es.wikipedia.org/wiki/Radio_(geometr%C3%ADa) 
	 * @param planets
	 * @param day
	 */
	private Point calculateSunRadio(List<Planet> planets, int day){
		List<Planet> planetsToday = calculatePlanetsCoordinatesByCurrentDay(planets,day);
		// Use the distance formula to calculate the lengths of the three sides of the triangle.
		// Formula to find the distance at two points.
		// Distance = √((x2 - x1)2 + (y2 - y 1)2)
		double distanceA = distanceformula(
								planetsToday.get(0).getCoordinates(), 
								planetsToday.get(1).getCoordinates());
		double distanceB = distanceformula(
								planetsToday.get(1).getCoordinates(), 
								planetsToday.get(2).getCoordinates());
		double distanceC = distanceformula(
								planetsToday.get(2).getCoordinates(), 
								planetsToday.get(0).getCoordinates());
		// Formula to calculate the ratio based on the three points.
		// Radio = (abc)/(√(a + b + c)(b + c - a)(c + a - b)(a + b - c))
		Point sunRadioCoordinates = new Point();
		sunRadioCoordinates.setxAxis(distanceA * distanceB * distanceC);
		sunRadioCoordinates.setyAxis(yAxisRatio(distanceA , distanceB , distanceC));
		return sunRadioCoordinates;
	}
	
	/**
	 * Formula to calculate the distance between two points.
	 * Distance = √((x2 - x1)2 + (y2 - y 1)2)
	 * @param a
	 * @param b
	 * @return The distance.
	 */
	private double distanceformula(Point a, Point b){
		double resultDistance = 
						Math.sqrt(
							Math.pow( 
								(b.getxAxis() - a.getxAxis()) , 2 
								) 
								+
							Math.pow( 
								(b.getyAxis() - a.getyAxis()) , 2 
								)
						)
					;
		
		return resultDistance;
	}
	
	/**
	 * Calculate the Planets Coordinates By the currentDay.
	 * @param planets
	 * @param day
	 * @return
	 */
	private List<Planet> calculatePlanetsCoordinatesByCurrentDay(List<Planet> planets, int day){
		List<Planet> planetToProcess = planets;
		// Coordinates points of planet to work.
		Point planetCoordinates1 = new Point();
		Point planetCoordinates2 = new Point();
		Point planetCoordinates3 = new Point();
		// Obtain the coordinates X of every point in the orbit of planets. 
		planetCoordinates1.setxAxis(
				calculateXAxisByCurrentDay(planetToProcess.get(0),day)
				);
		planetCoordinates2.setxAxis(
				calculateXAxisByCurrentDay(planetToProcess.get(1),day)
				);
		planetCoordinates3.setxAxis(
				calculateXAxisByCurrentDay(planetToProcess.get(2),day)
				);
		// Obtain the coordinates Y of every point in the orbit of planets. 
		planetCoordinates1.setyAxis(
				calculateYAxisByCurrentDay(planetToProcess.get(0),day)
				);
		planetCoordinates2.setyAxis(
				calculateYAxisByCurrentDay(planetToProcess.get(1),day)
				);
		planetCoordinates3.setyAxis(
				calculateYAxisByCurrentDay(planetToProcess.get(2),day)
				);
		// Set the calculated coordinates based on planet and current day.
		planetToProcess.get(0).setCoordinates(planetCoordinates1);
		planetToProcess.get(1).setCoordinates(planetCoordinates2);
		planetToProcess.get(2).setCoordinates(planetCoordinates3);
		
		return planetToProcess;
	}
	
	/**
	 * Base on the distances between three point .
	 * Ratio = (abc)/(√(a + b + c)(b + c - a)(c + a - b)(a + b - c))
	 * @param distanceA
	 * @param distanceB
	 * @param distanceC
	 * @return
	 */
	private double yAxisRatio(double distanceA ,double distanceB , double distanceC){
		double resultYAxisRatio = Math.sqrt(
								(
									(distanceA+distanceB+distanceC)
									*
									(distanceB+distanceC-distanceA)
									*
									(distanceC+distanceA-distanceB)
									*
									(distanceA+distanceB-distanceC)
								)
		);
		return resultYAxisRatio;
	}
	
	/**
	 * Calculate  the X Axis by the currentDay.
	 * @param planet
	 * @param day
	 * @return
	 */
	private double calculateXAxisByCurrentDay( Planet planet,int day){
		double xAxis = Math.cos(
							Math.toRadians(
									getDireccion(planet.isRotationClockwiseClockDirection())*
									(
										planet.getDegreeRotacionXday()*
										day
									)
							)
						);
		return xAxis;
	}
	
	/**
	 * Calculate  the Y Axis by the currentDay.
	 * @param planet
	 * @param day
	 * @return
	 */
	private double calculateYAxisByCurrentDay( Planet planet,int day){
		double yAxis = Math.sin(
							Math.toRadians(
									getDireccion(planet.isRotationClockwiseClockDirection())*
									(
										planet.getDegreeRotacionXday()*
										day
									)
							)
						);
		return yAxis;
	}
	
	/**
	 * Get the Clockwise ClockDirection of the planet.
	 * @param isRotationClockwiseClockDirection
	 * @return In favor 1 otherwise -1.
	 */
	public int getDireccion(boolean isRotationClockwiseClockDirection){
		return (isRotationClockwiseClockDirection)? 1:-1;
	}
	/**
	 * Calculate if is sun inside center planets' triangle.
	 * @see https://es.wikipedia.org/wiki/Circunferencia_circunscrita
	 * @param planets
	 * @param day
	 * @return
	 */
	private ResultPlanetsPosition calculateSunLocationVsTrianglePlanets(List<Planet> planets, int day , ResultPlanetsPosition result){
		List<Planet> planetsToday = calculatePlanetsCoordinatesByCurrentDay(planets,day);
		// Use the distance formula to calculate the lengths of the three sides of the triangle.
		// Formula to find the distance at two points.
		// Distance = √((x2 - x1)2 + (y2 - y 1)2)
		double sideTriangleA = distanceformula(
								planetsToday.get(0).getCoordinates(), 
								planetsToday.get(1).getCoordinates());
		double sideTriangleB = distanceformula(
								planetsToday.get(1).getCoordinates(), 
								planetsToday.get(2).getCoordinates());
		double sideTriangleC = distanceformula(
								planetsToday.get(2).getCoordinates(), 
								planetsToday.get(0).getCoordinates());

		// Calculate the internal angles triangle.
		double angleA = calculateCosAngleA(sideTriangleA, sideTriangleB,sideTriangleC);
		double angleB = calculateCosAngleC(sideTriangleA, sideTriangleB,sideTriangleC);
		double angleC = 180 - angleA - angleB;
		
		// Check the type of triangle to know the radio position.
		if (
			(angleA == 90) 
				|| 
				(angleB == 90)
					|| 
						(angleC == 90)
				){
			// Triángulo rectángulo, circuncentro en el punto medio de la hipotenusa.
			result.setSunInsideTrianglePlanets(true);
		}
		if (
				(angleA > 90) 
					|| 
					(angleB > 90)
						|| 
							(angleC > 90)
				){
			// Triángulo obtusángulo, circuncentro en el exterior del triángulo.
			result.setSunOutsideTrianglePlanets(true);
		}
		else if (
				(angleA < 90) 
					&& 
					(angleB < 90)
						&& 
							(angleC < 90)
				){
			// Triángulo acutángulo, circuncentro en interior del triángulo.
			result.setSunInsideTrianglePlanets(true);
		}
		else if (
					(angleA == 60) 
						&& 
						(angleB == 60)
							&& 
								(angleC == 60)
					){
				// Triángulo acutángulo, circuncentro en interior del triángulo.
				result.setSunInsideMaxRainTrianglePlanets(true);
		}else{
			
		}
		
		return result ;
	}
	
	/**
	 * Calculate Cos Angle from a side triangle.
	 * @param sideTriangle
	 * @return
	 */
	private double calculateCosAngleA(double sideTriangleA, double sideTriangleB, double sideTriangleC){
		
		double bca = (Math.pow(sideTriangleB, 2)
						+
						Math.pow(sideTriangleC, 2))
						-
						Math.pow(sideTriangleA, 2);
		double twobc = 2 * (sideTriangleB) * (sideTriangleC) ;
		
		
		double resultAngleCosA = bca / twobc;
		return  Math.toDegrees(Math.acos(resultAngleCosA));
	}
	
	/**
	 * Calculate Cos Angle from a side triangle.
	 * @param sideTriangle
	 * @return
	 */
	private double calculateCosAngleC(double sideTriangleA, double sideTriangleB, double sideTriangleC){
		double bca = (Math.pow(sideTriangleA, 2)
					+
					Math.pow(sideTriangleB, 2))
					-
					Math.pow(sideTriangleC, 2);
		
		double twobc = 2 * (sideTriangleA) * (sideTriangleB) ;
		
		double resultAngleCosC = bca / twobc;
		return  Math.toDegrees(Math.acos(resultAngleCosC));
	}

	
	
	@Override
	public void jobBatchServiceReport() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)+10);
		int daysToTenYears = daysToTenYears(new LocalDate());
		try{
			logger.info("Trabajando pronostico de planetas de 10 años por " + String.valueOf(daysToTenYears)+" dias.");
			List<ResultPlanetsPosition> resultPlanetsPositions = pronosticPlanetPositions(daysToTenYears);
			logger.info("Actualizando Pronostico en la base...");
			planetsPositionRepository.deleteAll();
			planetsPositionRepository.save(resultPlanetsPositions);
			logger.info("Pronostico de planetas de 10 años completo y actualizado en la base.");
		}catch (Exception e){
			logger.error("Error al intentar hacer el pronostico de planetas de 10 años y actualizar la base.");
		}
		
	}
}
